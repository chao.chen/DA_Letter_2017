% Plot results from Matlab, Python and Java

clc; clearvars; close all; rng('default');

N=[10 200:200:2000];
languages={'Matlab', 'C', 'Java', 'R', 'Python'};
examples=[1 2];

%% Convert .txt files saved by C to .mat
times=dlmread('./src_C/CExample1.txt');
save('./src_C/CExample1.mat','times');
times=dlmread('./src_C/CExample2.txt');
save('./src_C/CExample2.mat','times');

%% Plot the computational cost of the six algorithms
for ex=examples
    if ex == 3
        N = [2:9 10:10:50]
    end
    figure;
    set(gcf,'DefaulttextFontName','times new roman','DefaultaxesFontName','times new roman','DefaulttextFontAngle','italic');
    set(gcf, 'PaperUnits', 'inches');
    set(gcf, 'PaperSize', [8 6]);
    set(gcf, 'PaperPositionMode', 'manual');
    set(gcf, 'PaperPosition', [0 0 8 6]);
    set(gcf, 'renderer', 'painters');
    for l=1:length(languages)
        subplot(2,3,l);
        load(['src_' languages{l} '/' languages{l} 'Example' num2str(ex) '.mat']);
        if size(times, 1) == 11
            times = times';
        end
        times = times([2 3 5],:);
        p = plot(N, times', 'LineStyle', '-.', 'linewidth',1, 'markersize',4);
        markers = {'+','o','*','x','s','v','d','^','>','<','p','h'};
        linestyles = {'-', '--', ':', '-.'};
        for i = 1:length(p)
            p(i).Marker = markers{i};
        end
        xlabel('N'); ylabel('Computational cost (s)');
        title(languages{l},'fontsize',14);
        axis tight;
    end
    % h=legend('KM','EKM','EIASC', 'DA', 'DA*', 'DAND', 'location','eastoutside');
    h=legend('EKM','EIASC', 'DA*', 'location','eastoutside');
    P=get(gca,'position');
    set(h,'fontsize',10,'position',[P(1)+P(3)+.07 P(2)+.1 .1 .1]);
    % saveas(gcf,['Example' num2str(ex) '.jpg']);
    saveas(gcf,['Example' num2str(ex) '.pdf']);
end

