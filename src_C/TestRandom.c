//
// Created by Chao Chen on 09/11/2017.
//


#include <stdio.h>
#include "Algorithms/utils.h"
#include "Algorithms/da.h"
#include "Algorithms/da_star.h"
//#include "Algorithms/dand.h"
#include "Algorithms/eiasc.h"
#include "Algorithms/ekm.h"
#include "Algorithms/km.h"

int needSort = 0;
int s = 0;
int index0[H];
double Xs[M][H] = {{0.}};
double Fs[2 * M][H] = {{0.}};

int main() {
    int i, j, k;
    int n[N] = {0};
//    int p;
//    double temp;
    double results[6][M] = {{0.}};
    double times[6][N] = {{0.}};
    FILE *fp;
    n[0] = 10;
    for (i = 1; i < N; i++)
        n[i] = 200 * i;
    srand((unsigned) time(NULL));

    for (i = 0; i < N; i++) {
        printf("N[%d]=%d\n", i, n[i]);
        s = n[i];

        for (k = 0; k < M; k++) {
            for (j = 0; j < s; j++)
                Xs[k][j] = rand() % 10000 / 10000.0;
//            for (j = 0; j < s - 1; j++)
//                for (p = j + 1; p < s; p++)
//                    if (Xs[k][j] > Xs[k][p]) {
//                        temp = Xs[k][j];
//                        Xs[k][j] = Xs[k][p];
//                        Xs[k][p] = temp;
//                    }
            qsort(Xs[k], s, sizeof(double), qsortcmp);
        }

        for (j = 1; j < 2 * M; j += 2) {
            for (k = 0; k < s; k++) {
                Fs[j][k] = rand() % 10000 / 10000.0;
                Fs[j - 1][k] = (rand() % 10000 / 10000.0) * Fs[j][k];
            }
        }

        clock_t start, finish;
        double duration;
        start = clock();
        for (j = 0; j < M; j++)
            results[0][j] = KM(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[0][i] = duration;
        for (j = 0; j < 5; j++)
            printf("KM____y[%d]=%f\n", j, results[0][j]);
        printf("times=%f\n", times[0][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[1][j] = EKM(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[1][i] = duration;
        for (j = 0; j < 5; j++)
            printf("EKM___y[%d]=%f\n", j, results[1][j]);
        printf("times=%f\n", times[1][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[2][j] = EIASC(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[2][i] = duration;
        for (j = 0; j < 5; j++)
            printf("EIASC_y[%d]=%f\n", j, results[2][j]);
        printf("times=%f\n", times[2][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[3][j] = DA(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[3][i] = duration;
        for (j = 0; j < 5; j++)
            printf("DA____y[%d]=%f\n", j, results[3][j]);
        printf("times=%f\n", times[3][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[4][j] = da_star(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[4][i] = duration;
        for (j = 0; j < 5; j++)
            printf("da_star__y[%d]=%f\n", j, results[4][j]);
        printf("times=%f\n", times[4][i]);

/*
        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[5][j] = DAND(Xs[j], Xs[j], Fs[2 * j], Fs[2 * j + 1], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[5][i] = duration;
        for (j = 0; j < 5; j++)
            printf("DAND__y[%d]=%f\n", j, results[5][j]);
        printf("times=%f\n", times[5][i]);
*/
    }

    fp = fopen("../CExample2.txt", "w");
    if (fp == NULL) {
        printf("File cannot be openned! ");
        exit(0);
    }
    for (i = 0; i < N; i++)
        fprintf(fp, "%f\t%f\t%f\t%f\t%f\t%f\n", times[0][i], times[1][i],
                times[2][i], times[3][i], times[4][i], times[5][i]);

    fclose(fp);

    return 0;
}
