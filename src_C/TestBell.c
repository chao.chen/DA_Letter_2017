//
// Created by Chao Chen on 22/10/2017.
//

#include <stdio.h>
#include "Algorithms/utils.h"
#include "Algorithms/da.h"
#include "Algorithms/da_star.h"
//#include "Algorithms/dand.h"
#include "Algorithms/eiasc.h"
#include "Algorithms/ekm.h"
#include "Algorithms/km.h"

int needSort = 0;
int s = 0;
int index0[H];
double x[H] = {0.};
double mul[M][H] = {{0.}};
double muu[M][H] = {{0.}};

int main() {
    int i, j, k;
    int n[N] = {0};
    double b[M] = {0.};
    double al[M] = {0.};
    double au[M] = {0.};
    double c[M] = {0.};
    //double temp = 0.;
    double results[6][M] = {{0.}};
    double times[6][N] = {{0.}};
    FILE *fp;
    n[0] = 10;
    for (i = 1; i < N; i++)
        n[i] = 200 * i;
    srand((unsigned) time(NULL));
    for (i = 0; i < N; i++) {
        printf("N[%d]=%d\n", i, n[i]);
        s = n[i];
        for (j = 0; j < M; j++) {
            al[j] = rand() % 10000 / 10000.0 + 1;
            b[j] = rand() % 10000 / 10000.0 + 1;
            au[j] = al[j] * (rand() % 10000 / 10000.0 + 1);
            c[j] = 10 * (rand() % 10000 / 10000.0);
        }
        for (j = 0; j < s; j++) {
            x[j] = j * (10. / (s - 1));
        }
        for (j = 0; j < M; j++) {
            for (k = 0; k < s; k++) {
                mul[j][k] = 1 / (pow((fabs(x[k] - c[j]) / al[j]), (2 * b[j])) + 1);
                muu[j][k] = 1 / (pow((fabs(x[k] - c[j]) / au[j]), (2 * b[j])) + 1);
            }

        }

        clock_t start, finish;
        double duration;
        start = clock();
        for (j = 0; j < M; j++)
            results[0][j] = KM(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[0][i] = duration;
        for (j = 0; j < 5; j++)
            printf("KM____y[%d]=%f\n", j, results[0][j]);
        printf("times=%f\n", times[0][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[1][j] = EKM(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[1][i] = duration;
        for (j = 0; j < 5; j++)
            printf("EKM___y[%d]=%f\n", j, results[1][j]);
        printf("times=%f\n", times[1][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[2][j] = EIASC(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[2][i] = duration;
        for (j = 0; j < 5; j++)
            printf("EIASC_y[%d]=%f\n", j, results[2][j]);
        printf("times=%f\n", times[2][i]);

        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[3][j] = DA(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[3][i] = duration;
        for (j = 0; j < 5; j++)
            printf("DA____y[%d]=%f\n", j, results[3][j]);
        printf("times=%f\n", times[3][i]);


        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[4][j] = da_star(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[4][i] = duration;
        for (j = 0; j < 5; j++)
            printf("da_star__y[%d]=%f\n", j, results[4][j]);
        printf("times=%f\n", times[4][i]);


/*
        s = n[i];
        start = clock();
        for (j = 0; j < M; j++)
            results[5][j] = DAND(x, x, mul[j], muu[j], needSort);
        finish = clock();
        duration = (double) (finish - start) / CLOCKS_PER_SEC;
        times[5][i] = duration;
        for (j = 0; j < 5; j++)
            printf("DAND__y[%d]=%f\n", j, results[5][j]);
        printf("times=%f\n", times[5][i]);
*/
    }

    fp = fopen("../CExample1.txt", "w");
    if (fp == NULL) {
        printf("File cannot be openned! ");
        exit(0);
    }
    for (i = 0; i < N; i++)
        fprintf(fp, "%f\t%f\t%f\t%f\t%f\t%f\n", times[0][i], times[1][i],
                times[2][i], times[3][i], times[4][i], times[5][i]);

    fclose(fp);

    return 0;
}
