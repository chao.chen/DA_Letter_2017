//
// Created by Chao Chen on 22/10/2017.
//

#include "utils.h"

int qsortcmp(const void *a, const void *b) {
    if (*(double *) a > *(double *) b)
        return 1;
    else if (*(double *) a < *(double *) b)
        return -1;
    else
        return 0;
}

void heapSort(double *array, int size) {

    double *heapPointer = array - 1;
    int i, temp, index = 0;
    for (i = 0; i < size; i++)
        index0[i] = i;
    for (index = size / 2; index > 0; --index) {
        fixDown(heapPointer, index, size);
    }
    while (size > 1) {
        swap(&heapPointer[1], &heapPointer[size--]);
        temp = index0[0];
        index0[0] = index0[size];
        index0[size] = temp;
        fixDown(heapPointer, 1, size);
    }
}

void fixDown(double *array, int index, int size) {
    int temp, i = index;
    //int j = 2 * index;
    int j;
    while (2 * i <= size) {
        j = 2 * i;
        if (j + 1 <= size && array[j] < array[j + 1]) {
            ++j;
        }
        if (array[i] >= array[j]) {
            break;
        }
        swap(&array[i], &array[j]);
        temp = index0[i - 1];
        index0[i - 1] = index0[j - 1];
        index0[j - 1] = temp;
        i = j;
    }
}

void swap(double *value1, double *value2) {
    double tempValue = *value1;
    *value1 = *value2;
    *value2 = tempValue;
}


int roundi(double x) {
    double p;
    int y;
    p = x - (int) x;
    if (p < 0.5)
        y = (int) x;
    else
        y = (int) x + 1;
    return y;
}

int sign(int x) {
    int y;
    if (x == 0)y = 0;
    else y = x > 0 ? 1 : -1;
    return y;
}


