//
// Created by Chao Chen on 22/10/2017.
//

#include "utils.h"

double EKM(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort) {
    int i, j, l = -1, r = -1, minL, maxL, minR, maxR, ly, Lold, Rold, k = 0, count = 0, t = 0;
    int index[H];
    double yl, yr, y, epsilon, sum = 0., a = 0., b = 0.;
    double Xl_MIN = Xl0[0];
    double Xr_MAX = Xr0[0];
    double Wl_MAX = Wl0[0];
    double Xr2[H] = {0.};
    double Wl2[H] = {0.};
    double Wr2[H] = {0.};
    double f[H] = {0.};
    double delta[H] = {0.};
    double Xl[H] = {0.};
    double Xr[H] = {0.};
    double Wl[H] = {0.};
    double Wr[H] = {0.};
    ly = s;

    memcpy(Xl, Xl0, sizeof(double) * ly);
    memcpy(Xr, Xr0, sizeof(double) * ly);
    memcpy(Wl, Wl0, sizeof(double) * ly);
    memcpy(Wr, Wr0, sizeof(double) * ly);

    for (i = 0; i < ly; i++) {
        if (Wl_MAX < Wl[i])Wl_MAX = Wl[i];
        if (Xr_MAX < Xr[i])Xr_MAX = Xr[i];
        if (Xl_MIN > Xl[i])Xl_MIN = Xl[i];
    }
    if (Wl_MAX == 0) {
        yl = Xl_MIN;
        yr = Xr_MAX;
        y = (yl + yr) / 2;
        l = 1;
        r = ly - 1;
        return y;
    }

//    epsilon = pow(10, -10);
//    for (i = 0; i < ly; i++)
//        if (Wr[i] < epsilon) {
//            index[k++] = i;
//            t += i;
//        }
//    if (t == ly) {
//        yl = Xl_MIN;
//        yr = Xr_MAX;
//        y = (yl + yr) / 2;
//        l = 1;
//        r = ly - 1;
//        return y;
//    }
//    for (i = 0; i < k; i++) {
//        for (j = index[i]; j < ly; j++) {
//            Xl[j] = Xl[j + 1];
//            Xr[j] = Xr[j + 1];
//            Wl[j] = Wl[j + 1];
//            Wr[j] = Wr[j + 1];
//        }
//        ly--;
//    }

    for (i = k = 0; i < ly; i++) {
        if (Wr[i] != 0) {
            Xl[k] = Xl[i];
            Xr[k] = Xr[i];
            Wl[k] = Wl[i];
            Wr[k] = Wr[i];
            ++k;
        }
    }
    ly = k;

    //Compute yl
    if (needSort) {
        heapSort(Xl, ly);
        for (i = 0; i < ly; i++) {
            Xr2[i] = Xr[i];
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
        for (i = 0; i < ly; i++) {
            Xr[i] = Xr2[index0[i]];
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }

        for (i = 0; i < ly; i++) {
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
    }
    if (ly == 1) {
        yl = Xl[0];
        l = 1;
    } else {
        Lold = roundi(ly / 2.4);
        for (i = 0; i < Lold; i++) {
            f[i] = Wr[i];
            a += Xl[i] * f[i];
            b += f[i];
        }
        for (i = Lold; i < ly; i++) {
            f[i] = Wl[i];
            a += Xl[i] * f[i];
            b += f[i];
        }
        yl = a / b;
        for (i = 0; i < ly; i++) {
            if (Xl[i] > yl) {
                count = 1;
                break;
            }
        }
        if (count) l = i; else l = ly - 1;
        count = j = 0;
        while (l != Lold && j < 20) {
            if (l == 0) {
                l = 1;
                break;
            }
            minL = min(l, Lold);
            maxL = max(l, Lold);
            sum = 0.;
            for (i = minL; i < maxL; i++) {
                delta[i - minL] = sign(l - Lold) * (Wr[i] - Wl[i]);
                sum += delta[i - minL];
            }
            b += sum;
            for (i = minL; i < maxL; i++) {
                a += delta[i - minL] * Xl[i];
            }
            yl = a / b;
            Lold = l;
            for (i = 0; i < ly; i++) {
                if (Xl[i] > yl) {
                    count = 1;
                    break;
                }
            }
            if (count) l = i; else l = ly - 1;
            count = 0;
            j++;
        }
    }
    //Compute yr
    a = 0.;
    b = 0.;
    if (needSort == 1) {
        heapSort(Xr, ly);
        for (i = 0; i < ly; i++) {
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
    }
    if (ly == 1) {
        yr = Xr[0];
        r = 1;
    } else {
        Rold = roundi(ly / 1.7);
        for (i = 0; i < Rold; i++) {
            f[i] = Wl[i];
            a += Xr[i] * f[i];
            b += f[i];
        }
        for (i = Rold; i < ly; i++) {
            f[i] = Wr[i];
            a += Xr[i] * f[i];
            b += f[i];
        }
        yr = a / b;

        for (i = 0; i < ly; i++) {
            if (Xr[i] > yr) {
                count = 1;
                break;
            }
        }
        if (count)r = i; else r = ly - 1;
        count = j = 0;
        while (r != Rold && j < 20) {
            minR = min(r, Rold);
            maxR = max(r, Rold);
            sum = 0.;
            for (i = minR; i < maxR; i++) {
                delta[i - minR] = sign(r - Rold) * (Wr[i] - Wl[i]);
                sum += delta[i - minR];
            }
            b -= sum;
            for (i = minR; i < maxR; i++) {
                a -= delta[i - minR] * Xr[i];
            }
            yr = a / b;
            Rold = r;
            for (i = 0; i < ly; i++) {
                if (Xr[i] > yr) {
                    count = 1;
                    break;
                }
            }
            if (count) r = i; else r = ly - 1;
            count = 0;
            j++;
        }
    }
    y = (yl + yr) / 2;
    return y;
}
