//
// Created by Chao Chen on 22/10/2017.
//

#ifndef DA2017_DA_H
#define DA2017_DA_H

extern double DA(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort);

#endif //DA2017_DA_H
