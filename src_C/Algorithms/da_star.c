//
// Created by Chao Chen on 23/10/2017.
//

#include "utils.h"

double DA2C(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort) {
    int i, j, l, r, C, k;
//    int index[H];
    double yl, yr, y;
    double Xl_MIN = Xl0[0];
    double Xr_MAX = Xr0[0];
    double Wl_MAX = Wl0[0];
    double Xr2[H] = {0.};
    double Wl2[H] = {0.};
    double Wr2[H] = {0.};
//    double cusumPosW[H] = { 0. };
//    double cusumNegW[H] = { 0. };
    double deltaX[H - 1] = {0.};
//    double pos[H - 1] = { 0. };
//    double neg[H - 1] = { 0. };
//    double derivatives[H] = { 0. };
    double cusumpos[H] = {0.};
    double cusumneg[H] = {0.};
    double Xl[H] = {0.};
    double Xr[H] = {0.};
    double Wl[H] = {0.};
    double Wr[H] = {0.};

    int C2, ir;
    double cpw = 0., cnw = 0., cp = 0., cn = 0., derivative, sum, tmp;

    C = s;

    memcpy(Xl, Xl0, sizeof(double) * C);
    memcpy(Xr, Xr0, sizeof(double) * C);
    memcpy(Wl, Wl0, sizeof(double) * C);
    memcpy(Wr, Wr0, sizeof(double) * C);

    for (i = 0; i < C; i++) {
        if (Wl_MAX < Wl[i])Wl_MAX = Wl[i];
        if (Xr_MAX < Xr[i])Xr_MAX = Xr[i];
        if (Xl_MIN > Xl[i])Xl_MIN = Xl[i];
    }
    if (Wl_MAX == 0) {
        yl = Xl_MIN;
        yr = Xr_MAX;
        y = (yl + yr) / 2;
        l = 1;
        r = C - 1;
        return y;
    }

    for (i = k = 0; i < C; i++) {
        if (Wr[i] != 0) {
            Xl[k] = Xl[i];
            Xr[k] = Xr[i];
            Wl[k] = Wl[i];
            Wr[k] = Wr[i];
            ++k;
        }
    }
    C = k;

    //Compute yl
    if (needSort) {
        heapSort(Xl, C);
        for (i = 0; i < C; i++) {
            Xr2[i] = Xr[i];
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
        for (i = 0; i < C; i++) {
            Xr[i] = Xr2[index0[i]];
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
        for (i = 0; i < C; i++) {
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
    }
    if (C == 1) {
        yl = Xl[0];
        l = 1;
    } else {
        C2 = C - 1;

        for (i = 0, j = C2; i < C2; i++, j--) {
            ir = C2 - 1 - i;

            cpw += Wr[i];
            cnw += Wl[j];

            if (i <= ir) {
                deltaX[i] = Xl[i + 1] - Xl[i];
                deltaX[ir] = Xl[ir + 1] - Xl[ir];
            }

            //pos[i] = deltaX[i] * cpw;
            //neg[i] = deltaX[ir] * cnw;

            //cusumpos[i+1] = cp += pos[i];
            //cusumneg[ir] = cn += neg[i];

            cusumpos[i + 1] = cp += (deltaX[i] * cpw);
            cusumneg[ir] = cn += (deltaX[ir] * cnw);
        }

        for (i = 0; i < C; i++) {
            tmp = cusumpos[i] - cusumneg[i];
            if (tmp >= 0) break;
            derivative = tmp;
        }

        l = i == 0 ? 1 : i;

        sum = 0.;
        for (i = 0; i < l; i++) {
            sum += Wr[i];
        }
        for (i = l; i < C; i++) {
            sum += Wl[i];
        }

        yl = Xl[l - 1] - (derivative) / sum;

        //printf("C = %d, l = %d, yl=%f \n", C, l, yl);
    }

    //Compute yr
    if (needSort) {
        heapSort(Xr, C);
        for (i = 0; i < C; i++) {
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
    }
    if (C == 1) {
        yr = Xr[0];
        r = 1;
    } else {

        cpw = cnw = cp = cn = 0.;
        C2 = C - 1;

        for (i = 0, j = C2; i < C2; i++, j--) {
            ir = C2 - 1 - i;

            cpw += Wl[i];
            cnw += Wr[j];

            if (i <= ir) {
                deltaX[i] = Xr[i + 1] - Xr[i];
                deltaX[ir] = Xr[ir + 1] - Xr[ir];
            }

            //pos[i] = deltaX[i] * cpw;
            //neg[i] = deltaX[ir] * cnw;

            //cusumpos[i+1] = cp += pos[i];
            //cusumneg[ir] = cn += neg[i];

            cusumpos[i + 1] = cp += (deltaX[i] * cpw);
            cusumneg[ir] = cn += (deltaX[ir] * cnw);
        }

        for (i = 0; i < C; i++) {
            tmp = cusumpos[i] - cusumneg[i];
            if (tmp >= 0) break;
            derivative = tmp;
        }

        r = i == 0 ? 1 : i;

        sum = 0.;
        for (i = 0; i < r; i++) {
            sum += Wl[i];
        }
        for (i = r; i < C; i++) {
            sum += Wr[i];
        }

        yr = Xr[r - 1] - (derivative) / sum;
    }
    y = (yl + yr) / 2;
    return y;
}
