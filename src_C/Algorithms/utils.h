//
// Created by Chao Chen on 22/10/2017.
//

#ifndef DA2017_UTILS_H
#define DA2017_UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include "../def.h"
#include <string.h>

extern int s;
extern int index0[H];

extern int roundi(double x);

extern int sign(int x);

extern int qsortcmp(const void *a, const void *b);

extern void heapSort(double *array, int size);

extern void fixDown(double *array, int index, int size);

extern void swap(double *value1, double *value2);

#endif //DA2017_UTILS_H
