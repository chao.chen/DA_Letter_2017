//
// Created by Chao Chen on 22/10/2017.
//

#include "utils.h"

double EIASC(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort) {
    int i, l, r, ly, k;
//    int index[H];
    double yl, yr, y, a = 0., b = 0., t;
    double Xl_MIN = Xl0[0];
    double Xr_MAX = Xr0[0];
    double Wl_MAX = Wl0[0];
    double Xr2[H] = {0.};
    double Wl2[H] = {0.};
    double Wr2[H] = {0.};
    double Xl[H] = {0.};
    double Xr[H] = {0.};
    double Wl[H] = {0.};
    double Wr[H] = {0.};
    ly = s;

    memcpy(Xl, Xl0, sizeof(double) * ly);
    memcpy(Xr, Xr0, sizeof(double) * ly);
    memcpy(Wl, Wl0, sizeof(double) * ly);
    memcpy(Wr, Wr0, sizeof(double) * ly);

    for (i = 0; i < ly; i++) {
        if (Wl_MAX < Wl[i])Wl_MAX = Wl[i];
        if (Xr_MAX < Xr[i])Xr_MAX = Xr[i];
        if (Xl_MIN > Xl[i])Xl_MIN = Xl[i];
    }
    if (Wl_MAX == 0) {
        yl = Xl_MIN;
        yr = Xr_MAX;
        y = (yl + yr) / 2;
        l = 1;
        r = ly - 1;
        return y;
    }

    for (i = k = 0; i < ly; i++) {
        if (Wr[i] != 0) {
            Xl[k] = Xl[i];
            Xr[k] = Xr[i];
            Wl[k] = Wl[i];
            Wr[k] = Wr[i];
            ++k;
        }
    }
    ly = k;

    //Compute yl
    if (needSort) {
        heapSort(Xl, ly);
        for (i = 0; i < ly; i++) {
            Xr2[i] = Xr[i];
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
        for (i = 0; i < ly; i++) {
            Xr[i] = Xr2[index0[i]];
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
        for (i = 0; i < ly; i++) {
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
    }
    if (ly == 1) {
        yl = Xl[0];
        l = 1;
    } else {
        yl = Xl[ly - 1];
        l = 0;
        for (i = 0; i < ly; i++) {
            a += Xl[i] * Wl[i];
            b += Wl[i];
        }
        while (l < ly && yl > Xl[l]) {
            l++;
            t = Wr[l - 1] - Wl[l - 1];
            a += Xl[l - 1] * t;
            b += t;
            yl = a / b;
        }
    }
    //Compute yr
    a = 0.;
    b = 0.;
    if (needSort == 1) {
        heapSort(Xr, ly);
        for (i = 0; i < ly; i++) {
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
    }
    if (ly == 1) {
        yr = Xr[0];
        r = 1;
    } else {
        r = ly;
        yr = Xr[0];
        for (i = 0; i < ly; i++) {
            a += Xr[i] * Wl[i];
            b += Wl[i];
        }
        while (r > 0 && yr < Xr[r - 1]) {
            t = Wr[r - 1] - Wl[r - 1];
            a += Xr[r - 1] * t;
            b += t;
            yr = a / b;
            r = r - 1;
        }
    }
    y = (yl + yr) / 2;
    return y;
}
