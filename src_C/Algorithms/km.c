//
// Created by Chao Chen on 22/10/2017.
//

#include "utils.h"

double KM(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort) {
    int i, j, l, r, ly, k = 0, count = 0;
    int index[H];
    double yl, yr, y, epsilon, y0 = 0., sum = 0.;
    double Xl_MIN = Xl0[0];
    double Xr_MAX = Xr0[0];
    double Wl_MAX = Wl0[0];
    double Xr2[H] = {0.};
    double Wl2[H] = {0.};
    double Wr2[H] = {0.};
    double f[H] = {0.};
    double Xl[H] = {0.};
    double Xr[H] = {0.};
    double Wl[H] = {0.};
    double Wr[H] = {0.};
    ly = s;

    memcpy(Xl, Xl0, sizeof(double) * ly);
    memcpy(Xr, Xr0, sizeof(double) * ly);
    memcpy(Wl, Wl0, sizeof(double) * ly);
    memcpy(Wr, Wr0, sizeof(double) * ly);

    for (i = 0; i < ly; i++) {
        if (Wl_MAX < Wl[i])Wl_MAX = Wl[i];
        if (Xr_MAX < Xr[i])Xr_MAX = Xr[i];
        if (Xl_MIN > Xl[i])Xl_MIN = Xl[i];
    }
    if (Wl_MAX == 0) {
        yl = Xl_MIN;
        yr = Xr_MAX;
        y = (yl + yr) / 2;
        l = 1;
        r = ly - 1;
        return y;
    }

    epsilon = pow(10, -10);

//    for (i = 0; i < ly; i++)
//        if (Wr[i] < epsilon)index[k++] = i;
//    if (k == ly) {
//        yl = Xl_MIN;
//        yr = Xr_MAX;
//        y = (yl + yr) / 2;
//        l = 1;
//        r = ly - 1;
//        return y;
//    }
//    for (i = 0; i < k; i++) {
//        for (j = index[i]; j < ly; j++) {
//            Xl[j] = Xl[j + 1];
//            Xr[j] = Xr[j + 1];
//            Wl[j] = Wl[j + 1];
//            Wr[j] = Wr[j + 1];
//        }
//        ly--;
//    }

    for (i = k = 0; i < ly; i++) {
        if (Wr[i] != 0) {
            Xl[k] = Xl[i];
            Xr[k] = Xr[i];
            Wl[k] = Wl[i];
            Wr[k] = Wr[i];
            ++k;
        }
    }
    ly = k;

    //Compute yl
    if (needSort) {
        heapSort(Xl, ly);
        for (i = 0; i < ly; i++) {
            Xr2[i] = Xr[i];
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
        for (i = 0; i < ly; i++) {
            Xr[i] = Xr2[index0[i]];
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
        for (i = 0; i < ly; i++) {
            Wl2[i] = Wl[i];
            Wr2[i] = Wr[i];
        }
    }

    if (ly == 1) {
        yl = Xl[0];
        l = 1;
    } else {
        for (i = 0; i < ly; i++) {
            f[i] = (Wl[i] + Wr[i]) / 2;
            y0 += Xl[i] * f[i];
            sum += f[i];
        }
        y0 /= sum;
        yl = y0 + 1;
        k = 1;
        while (fabs(yl - y0) > epsilon && k <= ly) {
            yl = y0;
            k++;
            for (i = 0; i < ly; i++) {
                if (Xl[i] > y0) {
                    count = 1;
                    break;
                }
            }
            if (count) l = i; else l = ly - 1;
            count = 0;
            sum = 0.;
            y0 = 0.;
            for (i = 0; i < l; i++) {
                f[i] = Wr[i];
                y0 += Xl[i] * f[i];
                sum += f[i];
            }
            for (i = l; i < ly; i++) {
                f[i] = Wl[i];
                y0 += Xl[i] * f[i];
                sum += f[i];
            }
            y0 /= sum;
        }
    }
    //Compute yr
    if (needSort == 1) {
        heapSort(Xr, ly);
        for (i = 0; i < ly; i++) {
            Wl[i] = Wl2[index0[i]];
            Wr[i] = Wr2[index0[i]];
        }
    }

    y0 = 0.;
    count = 0;
    sum = 0.;

    if (ly == 1) {
        yr = Xr[0];
        r = 1;
    } else {
        for (i = 0; i < ly; i++) {
            f[i] = (Wl[i] + Wr[i]) / 2;
            y0 += Xr[i] * f[i];
            sum += f[i];
        }
        y0 /= sum;
        yr = y0 + 1;
        k = 1;
        while (fabs(yr - y0) > epsilon && k <= ly) {
            yr = y0;
            k++;
            for (i = 0; i < ly; i++) {
                if (Xr[i] > y0) {
                    count = 1;
                    break;
                }
            }
            if (count) r = i; else r = ly - 1;
            count = 0;
            y0 = 0.;
            sum = 0.;
            for (i = 0; i < r; i++) {
                f[i] = Wl[i];
                y0 += Xr[i] * f[i];
                sum += f[i];
            }
            for (i = r; i < ly; i++) {
                f[i] = Wr[i];
                y0 += Xr[i] * f[i];
                sum += f[i];
            }
            y0 /= sum;

        }
    }
    y = (yl + yr) / 2;//printf("%d+++%d\n",l,r);
    return y;
}

