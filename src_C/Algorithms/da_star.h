//
// Created by Chao Chen on 23/10/2017.
//

#ifndef DA2017_DA2C_H
#define DA2017_DA2C_H

extern double DA2C(double *Xl0, double *Xr0, double *Wl0, double *Wr0, int needSort);

#endif //DA2017_DA2C_H
