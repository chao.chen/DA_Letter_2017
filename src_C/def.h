//
// Created by Chao Chen on 22/10/2017.
//

#ifndef DA2017_DEF_H
#define DA2017_DEF_H

#define M 5000
#define N 11
#define H 2500
#define max(a, b) (a>b?a:b)
#define min(a, b) (a<b?a:b)

#define KK 50
#define NN 10
#define N2 100
#define len 13

#endif //DA2017_DEF_H
