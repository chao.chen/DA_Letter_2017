function [y,yl,yr,l,r]=EKM(Xl,Xr,Wl,Wr,needSort)

ly=length(Xl); XrEmpty=isempty(Xr);
if XrEmpty;  Xr=Xl; end
if max(Wl)==0
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2;  l=1; r=ly-1; return;
end
epsilon=10^(-10);
index=Wr<epsilon;
if sum(index)==ly
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2; l=1; r=ly-1; return;
end
Xl(index)=[]; Xr(index)=[];
Wl(index)=[]; Wr(index)=[];
ly=length(Xl);
if nargin==4;  needSort=1; end

% Compute yl
if  needSort
    [Xl,index]=sort(Xl); Xr=Xr(index);
    Wl=Wl(index); Wr=Wr(index);
    Wl2=Wl; Wr2=Wr;
end
if ly==1
    yl=Xl;  l=1;
else
    Lold=round(ly/2.4);
    f=[Wr(1:Lold) Wl(Lold+1:ly)];
    a=f*Xl'; b=sum(f); yl=a/b;
    l=find(Xl > yl,1,'first')-1;
    while l~=Lold
        if l==0; l=1; break; end
        minL=min(l,Lold);
        maxL=max(l,Lold);
        delta=sign(l-Lold)*(Wr(minL+1:maxL)-Wl(minL+1:maxL));
        b=b+sum(delta);
        a=a+delta*Xl(minL+1:maxL)';
        yl=a/b;      Lold=l;
        l=find(Xl > yl,1,'first')-1;
    end
end

% Compute yr
if ~XrEmpty && needSort==1
    [Xr,index]=sort(Xr);
    Wl=Wl2(index); Wr=Wr2(index);
end
if ly==1
    yr=Xr; r=1;
else
    Rold=round(ly/1.7);
    f=[Wl(1:Rold) Wr(Rold+1:ly)];
    
    a=f*Xr'; b=sum(f); yr=a/b;
    r=find(Xr > yr,1,'first')-1;
    
    while r~=Rold
        minR=min(r,Rold);
        maxR=max(r,Rold);
        delta=sign(r-Rold)*(Wr(minR+1:maxR)-Wl(minR+1:maxR));
        b=b-sum(delta);
        a=a-delta*Xr(minR+1:maxR)';
        yr=a/b;      Rold=r;
        r=find(Xr > yr,1,'first')-1;
        if isempty(r); r=ly-1; break; end
    end
end
y=(yl+yr)/2;
