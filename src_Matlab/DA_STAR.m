function [y,yl,yr,l,r]=DA_STAR(Xl,Xr,Wl,Wr,needSort)

N=length(Xl); XrEmpty=isempty(Xr);
if XrEmpty;  Xr=Xl; end
if max(Wl)==0
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2;  l=1; r=N-1; return;
end
index=Wr==0;
Xl(index)=[]; Xr(index)=[];
Wl(index)=[]; Wr(index)=[];
N=length(Xl);
if nargin==4;  needSort=1; end

% Compute yl
if  needSort
    [Xl,index]=sort(Xl); Xr=Xr(index);
    Wl=Wl(index); Wr=Wr(index);
    Wl2=Wl; Wr2=Wr;
end

if N==1
    yl=Xl;  l=1;
else

    cusumPosW=cumsum(Wr);
    cusumWl  =cumsum(Wl);
    cusumNegW=cusumWl(N) - cusumWl;

    deltaX=[diff(Xl) 0];

    pos = deltaX .* cusumPosW;
    neg = deltaX .* cusumNegW;

    sn = sum(neg);
    derivatives = cumsum(pos+neg);

    l=length(find(derivatives<sn)) + 1;
    if l >= N
        l = N - 1;
    end
    
    W = [Wr(1:l) Wl(1+l:N)];

    if l ~= 1
        yl = Xl(l) - (derivatives(l-1) - sn) / sum(W);
    else 
        yl = Xl(l) + sn/sum(W);
    end

end

% Compute yr
if ~XrEmpty && needSort==1
    [Xr,index]=sort(Xr);
    Wl=Wl2(index); Wr=Wr2(index);
end

if N==1
    yr=Xr; r=1;
else

    cusumPosW=cumsum(Wl);
    cusumWr  =cumsum(Wr);
    cusumNegW=cusumWr(N) - cusumWr;

    deltaX=[diff(Xr) 0];

    pos = deltaX .* cusumPosW;
    neg = deltaX .* cusumNegW;

    sn = sum(neg);
    derivatives = cumsum(pos+neg);

    r=length(find(derivatives<sn)) + 1;
    if r >= N
        r = N - 1;
    end
    
    W = [Wl(1:r) Wr(1+r:N)];

    if r ~= 1
        yr = Xr(r) - (derivatives(r-1) - sn) / sum(W);
    else 
        yr = Xr(r) + sn/sum(W);
    end

end
y=(yl+yr)/2;


