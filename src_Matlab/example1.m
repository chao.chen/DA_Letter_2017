% Example 1 in the DA paper

clc; clearvars; close all; rng('default');

m=5000;
N=[10 200:200:2000];
times=zeros(6,length(N));
results=nan(6,m);
needSort=0;

for k=1:length(N)
    N(k)
    
    al=1+rand(m,1);
    b=1+rand(m,1);
    au=al.*(1+rand(m,1));
    c=10*rand(m,1);
    
    %x=sort(10*rand(m,N(k)),2);
    x=linspace(0,10,N(k));
    
    mul=zeros(m,N(k)); muu=zeros(m,N(k));
    for i=1:m
        mul(i,:) = gbellmf(x, [al(i) b(i) c(i)]);
        muu(i,:) = gbellmf(x, [au(i) b(i) c(i)]);
    end
    
    tic;
    for i=1:m
    	%results(1,i)=KM(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
        results(1,i)=KM(x,x,mul(i,:),muu(i,:),needSort);
    end
    times(1,k)=toc;
    
    tic;
    for i=1:m
    	%results(2,i)=EKM(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
        results(2,i)=EKM(x,x,mul(i,:),muu(i,:),needSort);
    end
    times(2,k)=toc;
    
    tic;
    for i=1:m
    	%results(3,i)=EIASC(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
        results(3,i)=EIASC(x,x,mul(i,:),muu(i,:),needSort);
    end
    times(3,k)=toc;
    
    tic;
    for i=1:m
    	%results(4,i)=DA(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
        results(4,i)=DA(x,x,mul(i,:),muu(i,:),needSort);
    end
    times(4,k)=toc;
    
    tic;
    for i=1:m
    	%results(5,i)=DA_STAR(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
        results(5,i)=DA_STAR(x,x,mul(i,:),muu(i,:),needSort);
    end
    times(5,k)=toc;
    
%    tic;
%    for i=1:m
%    	%results(6,i)=DAND(x(i,:),x(i,:),mul(i,:),muu(i,:),needSort);
%        results(6,i)=DAND(x,x,mul(i,:),muu(i,:),needSort);
%    end
%    times(6,k)=toc;
end
save('MatlabExample1.mat','times','results');

%% Verify that the outputs of the four algorithms are the same
figure;
set(gcf,'DefaulttextFontName','times new roman','DefaultaxesFontName','times new roman','DefaulttextFontAngle','italic');
plot(results(:,1:100)');
xlabel('m'); ylabel('Defuzzified output');
legend('KM','EKM','EIASC', 'DA', 'DA*', 'DAND', 'location','northwest'); axis tight;


%% Plot the computational cost of the four algorithms
figure;
set(gcf,'DefaulttextFontName','times new roman','DefaultaxesFontName','times new roman','DefaulttextFontAngle','italic');
p = plot(N,times', 'LineStyle', '-.', 'linewidth',1, 'markersize',4);
markers = {'+','o','*','.','x','s','d','^','v','>','<','p','h'};
linestyles = {'-', '--', ':', '-.'};
for i = 1:length(p)
    p(i).Marker = markers{i};
    %    p(i).LineStyle = linestyles{i};
end
xlabel('N'); ylabel('Computational cost (s)');
legend('KM','EKM','EIASC', 'DA', 'DA*', 'DAND', 'location','northwest'); axis tight;
%saveas(gcf,'Example1.jpg');

