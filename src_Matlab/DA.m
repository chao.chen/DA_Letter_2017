function [y,yl,yr,l,r]=DA(Xl,Xr,Wl,Wr,needSort)

N=length(Xl); XrEmpty=isempty(Xr);
if XrEmpty;  Xr=Xl; end
if max(Wl)==0
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2;  l=1; r=N-1; return;
end
index=Wr==0;
Xl(index)=[]; Xr(index)=[];
Wl(index)=[]; Wr(index)=[];
N=length(Xl);
if nargin==4;  needSort=1; end

% Compute yl
if  needSort
    [Xl,index]=sort(Xl); Xr=Xr(index);
    Wl=Wl(index); Wr=Wr(index);
    Wl2=Wl; Wr2=Wr;
end
if N==1
    yl=Xl;  l=1;
else
    cusumPosW=cumsum(Wr(1:end-1));
    cusumNegW=cumsum(Wl(end:-1:2));

    deltaX=diff(Xl);

    pos = deltaX .* cusumPosW;
    neg = deltaX(end:-1:1) .* cusumNegW;

    derivatives = [0  cumsum(pos)] - [cumsum(neg(end:-1:1), 'reverse') 0];

    l=find(derivatives<0,1,'last');
    if isempty(l)
        l = 1;
    end

    yl=(Xl(1:l)*Wr(1:l)'+Xl(l+1:N)*Wl(1+l:N)')/(sum(Wr(1:l))+sum(Wl(1+l:N)));
%    yl = Xl * [Wr(1:l) Wl(1+l:end)]' / sum([Wr(1:l) Wl(1+l:end)]);
end



% Compute yr
if ~XrEmpty && needSort==1
    [Xr,index]=sort(Xr);
    Wl=Wl2(index); Wr=Wr2(index);
end
if N==1
    yr=Xr; r=1;
else
    cusumPosW=cumsum(Wl(1:end-1));
    cusumNegW=cumsum(Wr(end:-1:2));

    deltaX=diff(Xr);

    pos = deltaX .* cusumPosW;
    neg = deltaX(end:-1:1) .* cusumNegW;

    derivatives = [0  cumsum(pos)] - [cumsum(neg(end:-1:1), 'reverse') 0];

    r=find(derivatives<0,1,'last');
    if isempty(l)
        l = 1;
    end

    yr=(Xr(1:r)*Wl(1:r)'+Xr(r+1:N)*Wr(1+r:N)')/(sum(Wl(1:r))+sum(Wr(1+r:N)));
%    yr = Xr * [Wl(1:r) Wr(1+r:end)]' / sum([Wl(1:r) Wr(1+r:end)]);
end
y=(yl+yr)/2;


