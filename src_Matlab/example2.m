%% Example 2 in the DA paper

clearvars; close all; clc; rng('default');

%% Parameters
m=5000;
N=[10 200:200:2000];
times=zeros(6,length(N));
results=zeros(6,m);

%% Need to sort
for k=1:length(N)
    N(k)
    
    %needSort=1;    Xs=rand(m,N(k)); 
    needSort=0;     Xs=sort(rand(m,N(k)),2); 
    Fs=zeros(2*m,N(k));
    Fs(2:2:2*m,:)=rand(m,N(k));
    Fs(1:2:2*m-1,:)=rand(m,N(k)).*Fs(2:2:2*m,:);
    
    tic
    for j=1:m
        results(1,j)=KM(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
    end
    times(1,k)=toc;
    
    tic
    for j=1:m
        results(2,j)=EKM(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
    end
    times(2,k)=toc;
    
    tic
    for j=1:m
        results(3,j)=EIASC(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
    end
    times(3,k)=toc;
    
    tic
    for j=1:m
        results(4,j)=DA(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
    end
    times(4,k)=toc;
    
    tic
    for j=1:m
        results(5,j)=DA_STAR(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
    end
    times(5,k)=toc;
    
%    tic
%    for j=1:m
%        results(6,j)=DAND(Xs(j,:),Xs(j,:),Fs(2*j-1,:),Fs(2*j,:),needSort);
%    end
%    times(6,k)=toc;
end
save('MatlabExample2.mat','times','results');


%% Verify that the outputs of the four algorithms are the same
figure;
set(gcf,'DefaulttextFontName','times new roman','DefaultaxesFontName','times new roman','DefaulttextFontAngle','italic');
plot(results(:,1:100)');
xlabel('m'); ylabel('Defuzzified output');
legend('KM','EKM','EIASC', 'DA', 'DA*', 'DAND', 'location','northwest'); axis tight;

%%
figure;
set(gcf,'DefaulttextFontName','times new roman','DefaultaxesFontName','times new roman','DefaulttextFontAngle','italic');
p = plot(N,times', 'LineStyle', '-.', 'linewidth',1, 'markersize',4);
markers = {'+','o','*','.','x','s','d','^','v','>','<','p','h'};
linestyles = {'-', '--', ':', '-.'};
for i = 1:length(p)
    p(i).Marker = markers{i};
    %    p(i).LineStyle = linestyles{i};
end
xlabel('N'); ylabel('Computational cost (s)');
legend('KM','EKM','EIASC', 'DA', 'DA*', 'DAND', 'location','northwest'); axis tight;
%saveas(gcf,'Example2.jpg');




