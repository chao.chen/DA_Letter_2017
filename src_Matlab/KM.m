function [y,yl,yr,l,r] = KM(Xl,Xr,Wl,Wr,needSort)

ly=length(Xl); XrEmpty=isempty(Xr);
if XrEmpty;  Xr=Xl; end

if max(Wl)==0
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2;  l=1; r=ly-1; return;
end

epsilon=10^(-10);
index=find(Wr<epsilon);
if length(index)==ly
    yl=min(Xl); yr=max(Xr);
    y=(yl+yr)/2; l=1; r=ly-1; return;
end

Xl(index)=[]; Xr(index)=[];
Wl(index)=[]; Wr(index)=[];

if nargin==4;  needSort=1; end

% Compute yl
if  needSort
    [Xl,index]=sort(Xl); Xr=Xr(index);
    Wl=Wl(index); Wr=Wr(index);
end
Wl2=Wl; Wr2=Wr;

% % Make Xl unique
% for i=length(Xl):-1:2
%     if Xl(i)==Xl(i-1)
%         Wl(i)=Wl(i)+Wl(i-1);
%         Wr(i)=Wr(i)+Wr(i-1);
%         Wl(i-1)=[]; Wr(i-1)=[]; Xl(i)=[];
%     end
% end

ly=length(Xl);
if ly==1
    yl=Xl;  l=1;
else
    f = (Wl+Wr)/2 ;
    y0 = Xl*f'/sum(f);
    yl=y0+1; k=1;
    
    while abs(yl-y0)>epsilon && k<=ly
        yl = y0; k=k+1;
        l = find(Xl > y0,1,'first')-1;
        if isempty(l); l=ly-1; end
        f = [Wr(1:l) Wl(l+1:ly)];
        y0 = Xl*f'/sum(f);
    end
end

% Compute yr
if ~XrEmpty && needSort==1
    [Xr,index]=sort(Xr);
    Wl=Wl2(index); Wr=Wr2(index);
end

% % Make Xr unique
% if ~XrEmpty
%     for i=length(Xr):-1:2
%         if Xr(i)==Xr(i-1)
%             Wl(i)=Wl(i)+Wl(i-1);
%             Wr(i)=Wr(i)+Wr(i-1);
%             Wl(i-1)=[]; Wr(i-1)=[]; Xr(i)=[];
%         end
%     end
% end

ly=length(Xr);
if ly==1
    yr=Xr; r=1;
else
    f = (Wl+Wr)/2 ;
    y0 = Xr*f'/sum(f);
    yr=y0+1; k=1;
    
    while abs(yr-y0)>epsilon && k<=ly
        yr = y0; k=k+1;
        r = find(Xr > y0,1,'first')-1;
        if isempty(r); r=ly-1; end
        f = [Wl(1:r) Wr(r+1:ly)];
        y0 = Xr*f'/sum(f);
    end
end
y = (yl+yr)/2;