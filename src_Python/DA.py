import numpy as np


def DA(xl, wl, wr, needSort=True, xr=None):
    """
    xl: vector  1*length
    xr: vector  1*length
    wl: upper u
    wr: lower u
    needSort: needSort=True: input x need sort, else no sort
    """
    N = len(xl)
    if xr is None:
        # print('xr==None')
        xrEmt = True
        xr = xl
    else:
        xrEmt = False
    if np.max(wl) == 0:
        # print('np.max(wl)==0')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    index = wr == 0
    xl = xl[~index]
    xr = xr[~index]
    wl = wl[~index]
    wr = wr[~index]
    N = len(xl)
    if needSort == True:
        i_ = np.argsort(xl)
        xl = xl[i_]
        xr = xr[i_]
        wl = wl[i_]
        wr = wr[i_]
        wl2 = wl
        wr2 = wr
    if N == 1:
        yl = xl
        l = 0
    else:
        cusumPosW = np.cumsum(wr[0:(N - 1)])
        cusumNegW = np.cumsum(wl[(N - 1):0:-1])

        deltaX = np.diff(xl)

        pos = np.append(0, np.cumsum(deltaX * cusumPosW))
        neg = np.append(np.cumsum(deltaX[::-1] * cusumNegW)[::-1], 0)

        derivatives = pos - neg

        l = len(np.where(derivatives < 0)[0])
        if l == 0:
            l = 1

        yl = (np.dot(xl[:l], wr[:l]) + np.dot(xl[l:], wl[l:])) / (np.sum(wr[:l]) + np.sum(wl[l:]))

    # Compute yr
    if ~xrEmt and needSort == True:
        i_ = np.argsort(xr)
        xr = xr[i_]
        wl = wl2[i_]
        wr = wr2[i_]

    if N == 1:
        yr = xr
        r = 0
    else:
        cusumPosW = np.cumsum(wl[0:(N - 1)])
        cusumNegW = np.cumsum(wr[(N - 1):0:-1])

        deltaX = np.diff(xr)

        pos = deltaX * cusumPosW
        neg = deltaX[::-1] * cusumNegW

        derivatives = np.append(0, np.cumsum(pos)) - np.append(np.cumsum(neg)[::-1], 0)

        r = len(np.where(derivatives < 0)[0])
        if r == 0:
            r = 1

        yr = (np.dot(xr[:r], wl[:r]) + np.dot(xr[r:], wr[r:])) / (np.sum(wl[:r]) + np.sum(wr[r:]))

    y = (yl + yr) / 2
    return y, yl, yr, l, r
