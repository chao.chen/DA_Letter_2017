import time

import numpy as np
import scipy.io as sp

from DA import DA
from DA_STAR import DA_STAR
#from DAND import DAND
from EIASC import EIASC
from EKM import EKM
from KM import KM

n = np.linspace(200, 2000, (2000 - 200) / 200 + 1)
N = np.concatenate((np.array([10]), n)).astype(int)
m = 5000
times = np.zeros([6, len(N)])
results = np.zeros([6, m])
sort = False

for k in range(len(N)):
    print('n = %d' % N[k])
    Xs = np.random.rand(m, N[k])
    Xs = np.sort(Xs)
    Fs = np.zeros([2 * m, N[k]])
    Fs[1::2, :] = np.random.rand(m, N[k])
    Fs[::2] = np.random.rand(m, N[k]) * Fs[1::2, :]
    start = time.clock()
    for i in range(m):
        results[0, i], yl, yr, l, r = KM(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
    end = time.clock()
    times[0, k] = end - start
    start = time.clock()
    for i in range(m):
        results[1, i], yl, yr, l, r = EKM(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
    end = time.clock()
    times[1, k] = end - start
    start = time.clock()
    for i in range(m):
        results[2, i], yl, yr, l, r = EIASC(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
    end = time.clock()
    times[2, k] = end - start
    start = time.clock()
    for i in range(m):
        results[3, i], yl, yr, l, r = DA(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
    end = time.clock()
    times[3, k] = end - start
    start = time.clock()
    for i in range(m):
        results[4, i], yl, yr, l, r = DA_STAR(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
    end = time.clock()
    times[4, k] = end - start
#    start = time.clock()
#    for i in range(m):
#        results[5, i], yl, yr, l, r = DAND(Xs[i, :], Fs[2 * i], Fs[2 * i + 1], sort, xr=Xs[i, :])
#    end = time.clock()
#    times[5, k] = end - start
    print(times[:, k])

    # for i in range(6):
    #     print results[i, 0:5]
sp.savemat('PythonExample2.mat', {'times': times, 'results': results})
