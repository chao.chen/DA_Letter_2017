import numpy as np


def findFirst(ly, x, yl):
    for l in range(int(ly - 1)):
        if x[l] < yl and x[l + 1] >= yl:
            break
    return l


def KM(xl, wl, wr, needSort=True, xr=None):
    """
    xl: vector  1*length
    xr: vector  1*length
    wl: upper u
    wr: lower u
    needSort: needSort=True: input x need sort, else no sort
    """
    ly = len(xl)
    if xr is None:
        # print('xr==None')
        xrEmt = True
        xr = xl
    else:
        xrEmt = False
    if np.max(wl) == 0:
        # print('np.max(wl)==0')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    epsilon = 10e-10
    index = wr < epsilon
    # print np.sum(index)
    if np.sum(index) == ly:
        # print('np.sum(index)==ly')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    xl = xl[~index]
    xr = xr[~index]
    wl = wl[~index]
    wr = wr[~index]
    ly = len(xl)
    if needSort == True:
        i_ = np.argsort(xl)
        xl = xl[i_]
        xr = xr[i_]
        wl = wl[i_]
        wr = wr[i_]
        wl2 = wl
        wr2 = wr
    # # Make xl unique
    #    for i in range(1,ly)[::-1]:
    #        if xl[i]==xl[i-1]:
    #            wl[i]=wl[i]+wl[i-1]
    #            wr[i]=wr[i]+wr[i-1]
    #            wl=np.delete(wl,i-1)
    #            wr=np.delete(wr,i-1)
    #            xl=np.delete(xl,i)
    ly = len(xl)
    if ly == 1:
        yl = xl
        l = 0
    else:
        f = (wl + wr) / 2
        y0 = np.dot(xl, f.T) / np.sum(f)
        yl = y0 + 1
        k = 0
        while (np.abs(yl - y0) > epsilon and k < ly):
            yl = y0
            k += 1
            # l = findFirst(ly, xl, y0)
            l = np.sum(xl < y0) - 1
            if l < 0:
                l = 0
            f = np.concatenate((wr[0:l + 1], wl[l + 1:ly]))
            y0 = np.dot(xl, f.T) / np.sum(f)

    if ~xrEmt and needSort == True:
        i_ = np.argsort(xr)
        xr = xr[i_]
        wl = wl2[i_]
        wr = wr2[i_]
    # # Make xr unique
    #    if ~xrEmt:
    #        for i in range(1,len(xr))[::-1]:
    #            if xr[i]==xr[i-1]:
    #                wl[i]=wl[i]+wl[i-1]
    #                wr[i]=wr[i]+wr[i-1]
    #                wl=np.delete(wl,i-1)
    #                wr=np.delete(wr,i-1)
    #                xr=np.delete(xr,i)
    ly = len(xr)
    if ly == 1:
        yr = xr
        r = 0
    else:
        f = (wl + wr) / 2
        y0 = np.dot(xr, f.T) / np.sum(f)
        yr = y0 + 1
        k = 0
        while (np.abs(yr - y0) > epsilon and k < ly):
            yr = y0
            k += 1
            # r = findFirst(ly, xr, y0)
            r = np.sum(xr < y0) - 1
            if r < 0:
                r = 0
            f = np.concatenate((wl[:r + 1], wr[r + 1:ly]))
            y0 = np.dot(xr, f.T) / np.sum(f)
    y = (yl + yr) / 2
    return y, yl, yr, l, r
