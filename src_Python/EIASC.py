import numpy as np


def EIASC(xl, wl, wr, needSort=True, xr=None):
    """
    xl: vector  1*length
    xr: vector  1*length
    wl: upper u
    wr: lower u
    needSort: needSort=True: input x need sort, else no sort
    """
    ly = len(xl)
    if xr is None:
        # print('xr==None')
        xrEmt = True
        xr = xl
    else:
        xrEmt = False
    if np.max(wl) == 0:
        # print('np.max(wl)==0')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    epsilon = 10e-10
    index = wr < epsilon
    # print np.sum(index)
    if np.sum(index) == ly:
        # print('np.sum(index)==ly')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    xl = xl[~index]
    xr = xr[~index]
    wl = wl[~index]
    wr = wr[~index]
    # ly=xl.shape[1]
    ly = len(xl)
    # Compute yl
    if needSort == True:
        i_ = np.argsort(xl)
        xl = xl[i_]
        xr = xr[i_]
        wl = wl[i_]
        wr = wr[i_]
        wl2 = wl
        wr2 = wr
    if ly == 1:
        yl = xl
        l = 0
    else:
        yl = xl[-1]
        l = -1
        a = np.dot(xl, wl.T)
        b = np.sum(wl)
        while (l < ly and yl > xl[l + 1]):
            l += 1
            t = wr[l] - wl[l]
            a += xl[l] * t
            b += t
            yl = a / b
    # Compute yr
    if ~xrEmt and needSort == True:
        i_ = np.argsort(xr)
        xr = xr[i_]
        wl = wl2[i_]
        wr = wr2[i_]
    if ly == 1:
        yr = xr
        r = 0
    else:
        r = ly - 1
        yr = xr[0]
        a = np.dot(xr, wl.T)
        b = np.sum(wl)
        while (r >= 0 and yr < xr[r]):
            t = wr[r] - wl[r]
            a += xr[r] * t
            b += t
            yr = a / b
            r = r - 1
    y = (yl + yr) / 2
    return y, yl, yr, l, r
