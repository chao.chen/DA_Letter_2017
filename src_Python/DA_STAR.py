import numpy as np


def DA_STAR(xl, wl, wr, needSort=True, xr=None):
    N = len(xl)
    if xr is None:
        # print('xr==None')
        xrEmt = True
        xr = xl
    else:
        xrEmt = False
    if np.max(wl) == 0:
        # print('np.max(wl)==0')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    index = wr == 0
    xl = xl[~index]
    xr = xr[~index]
    wl = wl[~index]
    wr = wr[~index]
    N = len(xl)
    # Compute yl
    if needSort == True:
        i_ = np.argsort(xl)
        xl = xl[i_]
        xr = xr[i_]
        wl = wl[i_]
        wr = wr[i_]
        wl2 = wl
        wr2 = wr
    if N == 1:
        yl = xl
        l = 0
    else:
        cumsumPosW = np.cumsum(wr)
        cumsumWl = np.cumsum(wl)
        cumsumNegW = cumsumWl[N - 1] - cumsumWl

        deltaX = np.append(np.diff(xl), 0)

        pos = deltaX * cumsumPosW
        neg = deltaX * cumsumNegW
        sn = np.sum(neg)
        derivatives = np.cumsum(pos + neg)

        l = np.sum(derivatives < sn) + 1
        if l >= N:
            l = N - 1

        sw = np.sum(wr[:l]) + np.sum(wl[l:])

        if l != 1:
            yl = xl[l - 1] - (derivatives[l - 2] - sn) / sw
        else:
            yl = xl[l - 1] + sn / sw
    # Compute yr
    if ~xrEmt and needSort == True:
        i_ = np.argsort(xr)
        xr = xr[i_]
        wl = wl2[i_]
        wr = wr2[i_]
    if N == 1:
        yr = xr
        r = 0
    else:
        cumsumPosW = np.cumsum(wl)
        cumsumWr = np.cumsum(wr)
        cumsumNegW = cumsumWr[N - 1] - cumsumWr

        deltaX = np.concatenate((np.diff(xr), np.array([0])))

        pos = deltaX * cumsumPosW
        neg = deltaX * cumsumNegW
        sn = np.sum(neg)
        derivatives = np.cumsum(pos + neg)

        r = len(np.where(derivatives < sn)[0]) + 1
        if r >= N:
            r = N - 1

        sw = np.sum(wl[:r]) + np.sum(wr[r:])

        if r != 0:
            yr = xr[r - 1] - (derivatives[r - 2] - sn) / sw
        else:
            yr = xr[r - 1] + sn / sw
    y = (yl + yr) / 2
    return y, yl, yr, l, r
