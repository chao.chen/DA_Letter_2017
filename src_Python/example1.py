import time

import numpy as np
import scipy.io as sp

from DA import DA
from DA_STAR import DA_STAR
#from DAND import DAND
from EIASC import EIASC
from EKM import EKM
from KM import KM

n = np.linspace(200, 2000, (2000 - 200) / 200 + 1)
N = np.concatenate((np.array([10]), n)).astype(int)
m = 5000
times = np.zeros([6, len(N)])
results = np.zeros([6, m])
sort = False
for k in range(len(N)):
    print('n = %d' % N[k])
    al = 1 + np.random.rand(m, 1)
    b = np.ones([m, 1])
    au = al * (1 + np.random.rand(m, 1))
    c = 10 * np.random.rand(m, 1)
    x = np.linspace(0, 10, N[k])
    mul = np.zeros([m, N[k]])
    muu = np.zeros([m, N[k]])
    for i in range(m):
        mul[i, :] = 1 / (1 + ((x - c[i]) / al[i]) ** (2 * b[i]))
        muu[i, :] = 1 / (1 + ((x - c[i]) / au[i]) ** (2 * b[i]))
    start = time.clock()
    for i in range(m):
        results[0, i], yl, yr, l, r = KM(x, mul[i, :], muu[i, :], sort, xr=x)
    end = time.clock()
    times[0, k] = end - start
    start = time.clock()
    for i in range(m):
        results[1, i], yl, yr, l, r = EKM(x, mul[i, :], muu[i, :], sort, xr=x)
    end = time.clock()
    times[1, k] = end - start
    start = time.clock()
    for i in range(m):
        results[2, i], yl, yr, l, r = EIASC(x, mul[i, :], muu[i, :], sort, xr=x)
    end = time.clock()
    times[2, k] = end - start
    start = time.clock()
    for i in range(m):
        results[3, i], yl, yr, l, r = DA(x, mul[i, :], muu[i, :], sort, xr=x)
    end = time.clock()
    times[3, k] = end - start
    start = time.clock()
    for i in range(m):
        results[4, i], yl, yr, l, r = DA_STAR(x, mul[i, :], muu[i, :], sort, xr=x)
    end = time.clock()
    times[4, k] = end - start
#    start = time.clock()
#    for i in range(m):
#        results[5, i], yl, yr, l, r = DAND(x, mul[i, :], muu[i, :], sort, xr=x)
#    end = time.clock()
#    times[5, k] = end - start
    print(times[:, k])

    # for i in range(6):
    #     print results[i, 0:5]
sp.savemat('PythonExample1.mat', {'times': times, 'results': results})
