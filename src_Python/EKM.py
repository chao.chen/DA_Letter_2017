import numpy as np


# def findFirstwithNone(ly, x, yl):
#     for l in range(int(ly - 1)):
#         if x[l] < yl and x[l + 1] >= yl:
#             break
#     if l == int(ly - 2):
#         return None
#     return l
#
#
# def findFirst(ly, x, yl):
#     for l in range(int(ly - 1)):
#         if x[l] < yl and x[l + 1] >= yl:
#             break
#     return l


def EKM(xl, wl, wr, needSort=True, xr=None):
    """
    xl: vector  1*length
    xr: vector  1*length
    wl: upper u
    wr: lower u
    needSort: needSort=True: input x need sort, else no sort
    """
    ly = len(xl)
    if xr is None:
        # print('xr==None')
        xrEmt = True
        xr = xl
    else:
        xrEmt = False
    if np.max(wl) == 0:
        # print('np.max(wl)==0')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    epsilon = 10e-10
    index = wr < epsilon
    if np.sum(index) == ly:
        # print('np.sum(index)==ly')
        yl = np.min(xl)
        yr = np.max(xr)
        y = (yl + yr) / 2
        l = 0
        r = ly - 1
        return y, yl, yr, l, r
    xl = xl[~index]
    xr = xr[~index]
    wl = wl[~index]
    wr = wr[~index]
    # ly=xl.shape[1]
    ly = len(xl)
    # Compute yl
    if needSort == True:
        i_ = np.argsort(xl)
        xl = xl[i_]
        xr = xr[i_]
        wl = wl[i_]
        wr = wr[i_]
        wl2 = wl
        wr2 = wr
    if ly == 1:
        yl = xl
        l = 0
    else:
        Lold = int(np.round(ly / 2.4) - 1)  # For index begin with 0 in python
        f = np.concatenate((wr[0:Lold + 1], wl[Lold + 1:ly]))
        a = np.dot(f, xl.T)
        b = np.sum(f)
        yl = a / b
        # l = findFirst(ly, xl, yl)
        l = np.sum(xl < yl) - 1
        if l < 0:
            l = 0
        j = 0
        while l != Lold and j < 20:
            if l == -1:
                l = 0
                break
            minL = int(np.min([l, Lold]))
            maxL = int(np.max([l, Lold]))
            delta = np.sign(l - Lold) * (wr[minL + 1:maxL + 1] - wl[minL + 1:maxL + 1])
            b = b + np.sum(delta)
            a = a + np.dot(delta, xl[minL + 1:maxL + 1].T)
            if b == 0:
                break;
            yl = a / b
            Lold = l

            # l = findFirst(ly, xl, yl)
            l = np.sum(xl < yl) - 1
            if l < 0:
                l = 0
            j = j + 1
    # Compute yr
    if ~xrEmt and needSort == True:
        i_ = np.argsort(xr)
        xr = xr[i_]
        wl = wl2[i_]
        wr = wr2[i_]
    if ly == 1:
        yr = xr
        r = 0
    else:
        Rold = int(np.round(ly / 1.7) - 1)  # For index begin with 0 in python
        f = np.concatenate((wl[0:Rold + 1], wr[Rold + 1:ly]))
        a = np.dot(f, xr.T)
        b = np.sum(f)
        yr = a / b
        # r = findFirst(ly, xr, yr)
        r = np.sum(xr < yr) - 1
        if r < 0:
            r = 0
        j = 0
        while r != Rold and j < 20:
            minR = int(np.min([r, Rold]))
            maxR = int(np.max([r, Rold]))
            delta = np.sign(r - Rold) * (wr[minR + 1:maxR + 1] - wl[minR + 1:maxR + 1])
            b = b - np.sum(delta)
            a = a - np.dot(delta, xr[minR + 1:maxR + 1].T)
            if b == 0:
                break;
            yr = a / b
            Rold = r
            # r = findFirstwithNone(ly, xr, yr)
            # if r is None:
            #     r = ly - 1
            #     break
            r = np.sum(xr < yr) - 1
            if r < 0:
                r = 0
            j = j + 1
    y = (yl + yr) / 2
    return y, yl, yr, l, r
