package Algorithms;

public class DA_STAR {

    public static double[] run(double[] xl, double[] xr, double[] wl, double[] wr, boolean sort) {
        double y;
        double yl;
        double yr;
        int l;
        int r;
        double[] wl2 = null;
        double[] wr2 = null;
        int ly = xl.length;

        int C2, i, j, ir;
        double cpw = 0., cnw = 0., cp = 0., cn = 0., derivative = 0, sum, tmp;
        double[] deltaX = new double[ly];
        double[] cusumpos = new double[ly];
        double[] cusumneg = new double[ly];


        if (utils.max(wl) == 0) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        double epsilon = 10e-10f;
        int[] index = utils.find(wr, epsilon, false);
        if (index.length == ly) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        xl = utils.delete(xl, index);
        xr = utils.delete(xr, index);
        wl = utils.delete(wl, index);
        wr = utils.delete(wr, index);
        ly = xl.length;
        if (sort) {
            int[] i_ = utils.argSort(xl);
            xl = utils.sortByIndex(xl, i_);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr, i_);
            wl = utils.sortByIndex(wl, i_);
            wl2 = wl.clone();
            wr2 = wr.clone();
        }
        if (ly == 1) {
            yl = xl[0];
            l = 0;
        } else {
            C2 = ly - 1;

            for (i = 0, j = C2; i < C2; i++, j--) {
                ir = C2 - 1 - i;

                cpw += wr[i];
                cnw += wl[j];

                if (i <= ir) {
                    deltaX[i] = xl[i + 1] - xl[i];
                    deltaX[ir] = xl[ir + 1] - xl[ir];
                }

                //pos[i] = deltaX[i] * cpw;
                //neg[i] = deltaX[ir] * cnw;

                //cusumpos[i+1] = cp += pos[i];
                //cusumneg[ir] = cn += neg[i];

                cusumpos[i + 1] = cp += (deltaX[i] * cpw);
                cusumneg[ir] = cn += (deltaX[ir] * cnw);
            }

            for (i = 0; i < ly; i++) {
                tmp = cusumpos[i] - cusumneg[i];
                if (tmp >= 0) break;
                derivative = tmp;
            }

            l = i == 0 ? 1 : i;

            sum = 0.;
            for (i = 0; i < l; i++) {
                sum += wr[i];
            }
            for (i = l; i < ly; i++) {
                sum += wl[i];
            }

            yl = xl[l - 1] - (derivative) / sum;
        }
        if (sort) {
            int[] i_ = utils.argSort(xr);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr2, i_);
            wl = utils.sortByIndex(wl2, i_);
        }
        if (ly == 1) {
            yr = xr[0];
            r = 0;
        } else {
            cpw = cnw = cp = cn = 0.;
            C2 = ly - 1;

            for (i = 0, j = C2; i < C2; i++, j--) {
                ir = C2 - 1 - i;

                cpw += wl[i];
                cnw += wr[j];

                if (i <= ir) {
                    deltaX[i] = xr[i + 1] - xr[i];
                    deltaX[ir] = xr[ir + 1] - xr[ir];
                }

                //pos[i] = deltaX[i] * cpw;
                //neg[i] = deltaX[ir] * cnw;

                //cusumpos[i+1] = cp += pos[i];
                //cusumneg[ir] = cn += neg[i];

                cusumpos[i + 1] = cp += (deltaX[i] * cpw);
                cusumneg[ir] = cn += (deltaX[ir] * cnw);
            }

            for (i = 0; i < ly; i++) {
                tmp = cusumpos[i] - cusumneg[i];
                if (tmp >= 0) break;
                derivative = tmp;
            }

            r = i == 0 ? 1 : i;

            sum = 0.;
            for (i = 0; i < r; i++) {
                sum += wl[i];
            }
            for (i = r; i < ly; i++) {
                sum += wr[i];
            }

            yr = xr[r - 1] - (derivative) / sum;
        }

        y = (yl + yr) / 2;

        return new double[]{y, yl, yr, (double) l, (double) r};

    }

}
