package Algorithms;

public class EIASC {

    public static double[] run(double[] xl, double[] xr, double[] wl, double[] wr, boolean sort) {
        double y;
        double yl;
        double yr;
        int l = 0;
        int r = 0;
        double[] wl2 = null;
        double[] wr2 = null;
        int ly = xl.length;
        if (utils.max(wl) == 0) {

            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        double epsilon = 10e-10f;
        int[] index = utils.find(wr, epsilon, false);
        if (index.length == ly) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        xl = utils.delete(xl, index);
        xr = utils.delete(xr, index);
        wl = utils.delete(wl, index);
        wr = utils.delete(wr, index);
        ly = xl.length;
        if (sort) {
            int[] i_ = utils.argSort(xl);
            xl = utils.sortByIndex(xl, i_);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr, i_);
            wl = utils.sortByIndex(wl, i_);
            wl2 = wl.clone();
            wr2 = wr.clone();
        }
        if (ly == 1) {
            yl = xl[0];
            l = 0;
        } else {
            yl = xl[xl.length - 1];
            l = -1;
            double a = utils.times(xl, wl);
            double b = utils.sum(wl);
            while (l < ly && yl > xl[l + 1]) {
                l++;
                double t = wr[l] - wl[l];
                a += xl[l] * t;
                b += t;
                yl = a / b;
            }
        }
        if (sort) {
            int[] i_ = utils.argSort(xr);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr2, i_);
            wl = utils.sortByIndex(wl2, i_);
        }
        if (ly == 1) {
            yr = xr[0];
            r = 0;
        } else {
            r = ly - 1;
            yr = xr[0];
            double a = utils.times(xr, wl);
            double b = utils.sum(wl);
            while (r >= 0 && yr < xr[r]) {
                double t = wr[r] - wl[r];
                a += xr[r] * t;
                b += t;
                yr = a / b;
                r = r - 1;
            }
        }

        y = (yl + yr) / 2;

        return new double[]{y, yl, yr, (double) l, (double) r};
    }
}
