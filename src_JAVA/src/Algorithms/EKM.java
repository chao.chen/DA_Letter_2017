package Algorithms;

public class EKM {

    public static double[] run(double[] xl, double[] xr, double[] wl, double[] wr, boolean sort) {
        double y;
        double yl;
        double yr;
        int l = 0;
        int r = 0;
        double[] wl2 = null;
        double[] wr2 = null;
        int ly = xl.length;
        if (utils.max(wl) == 0) {

            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        double epsilon = 10e-10f;
        int[] index = utils.find(wr, epsilon, false);
        if (index.length == ly) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        xl = utils.delete(xl, index);
        xr = utils.delete(xr, index);
        wl = utils.delete(wl, index);
        wr = utils.delete(wr, index);
        ly = xl.length;
        if (sort) {
            int[] i_ = utils.argSort(xl);
            xl = utils.sortByIndex(xl, i_);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr, i_);
            wl = utils.sortByIndex(wl, i_);
            wl2 = wl.clone();
            wr2 = wr.clone();
        }
        if (ly == 1) {
            yl = xl[0];
            l = 0;
        } else {
            int Lold = (int) (Math.round(ly / 2.4) - 1);
            double[] f = utils.connect(utils.part(wr, 0, Lold + 1), utils.part(wl, Lold + 1, ly));
            double a = utils.times(f, xl);
            double b = utils.sum(f);
            yl = a / b;
            l = utils.findFirst(xl, yl);
            while (l != Lold) {
                if (l == -1) {
                    l = 0;
                    break;
                }
                int minL = utils.min(new int[]{l, Lold});
                int maxL = utils.max(new int[]{l, Lold});
                double[] delta = utils.dotTimes(Math.signum(l - Lold), utils.minus(utils.part(wr, minL + 1, maxL + 1), utils.part(wl, minL + 1, maxL + 1)));
                b += utils.sum(delta);
                a += utils.times(delta, utils.part(xl, minL + 1, maxL + 1));
                yl = a / b;
                Lold = l;
                l = utils.findFirst(xl, yl);


            }
        }
        if (sort) {
            int[] i_ = utils.argSort(xr);
            xr = utils.sortByIndex(xr, i_);
            xl = utils.sortByIndex(xl, i_);
            wr = utils.sortByIndex(wr2, i_);
            wl = utils.sortByIndex(wl2, i_);
        }
        if (ly == 1) {
            yr = xr[0];
            r = 0;
        } else {
            int Rold = (int) Math.round(ly / 1.7 - 1);
            double[] f = utils.connect(utils.part(wl, 0, Rold + 1), utils.part(wr, Rold + 1, ly));
            double a = utils.times(f, xr);
            double b = utils.sum(f);
            yr = a / b;
            r = utils.findFirst(xr, yr);
            while (r != Rold) {
                int minR = utils.min(new int[]{r, Rold});
                int maxR = utils.max(new int[]{r, Rold});
                double[] delta = utils.dotTimes(Math.signum(r - Rold),
                        utils.minus(utils.part(wr, minR + 1, maxR + 1),
                                utils.part(wl, minR + 1, maxR + 1)));
                b -= utils.sum(delta);
                a -= utils.times(delta, utils.part(xr, minR + 1, maxR + 1));
                yr = a / b;
                Rold = r;
                r = utils.findFirst(xr, yr);
                if (r == -1) {
                    r = ly - 1;
                    break;
                }
            }
        }

        y = (yl + yr) / 2;

        return new double[]{y, yl, yr, (double) l, (double) r};
    }
}
