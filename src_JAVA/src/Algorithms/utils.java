package Algorithms;

import java.util.ArrayList;
import java.util.List;

public class utils {
    public static void printArray(int[] tt) {
        for (int i = 0; i < tt.length; i++) {
            System.out.print(tt[i] + "\t");
        }
        System.out.print("\n");
    }

    public static void printArray(double[] tt) {
        for (int i = 0; i < tt.length; i++) {
            System.out.print(tt[i] + "\t");
        }
        System.out.print("\n");
    }

    public static void printArray(double[] x, int a, int b) {
        for (int i = a; i < b; i++) {
            System.out.print(x[i] + "\t");
        }
        System.out.print("\n");
    }

    public static double[] dotPlus(double a, double[] b) {
        double[] res = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            res[i] = a + b[i];
        }
        return res;
    }

    public static double[][] dotPlus(double a, double[][] b) {
        double[][] res = new double[b.length][b[0].length];
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                res[i][j] = a + b[i][j];
            }
        }
        return res;
    }

    public static double[] dotPlus(double[] a, double[] b) {
        double[] res = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            res[i] = a[i] + b[i];
        }
        return res;
    }

    public static double[] rand(int length) {
        java.util.Random r = new java.util.Random();
        double[] l = new double[length];
        for (int i = 0; i < length; i++) {
            l[i] = r.nextDouble();
        }
        return l;
    }

    public static double[][] rand(int x, int y) {
        java.util.Random r = new java.util.Random();
        double[][] l = new double[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                l[i][j] = r.nextDouble();
            }
        }
        return l;
    }

    public static double[] dotTimes(double[] a, double[] b) {
        if (a.length != b.length) {
            System.out.println("Input arrays' shape don't match");
            System.exit(1);
            return null;
        }
        double[] c = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i] * b[i];
        }
        return c;
    }

    public static double[] dotTimes(double a, double[] b) {

        double[] c = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = a * b[i];
        }
        return c;
    }

    public static double[][] dotTimes(double a, double[][] b) {

        double[][] c = new double[b.length][b[0].length];
        for (int i = 0; i < b.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                c[i][j] = a * b[i][j];
            }
        }
        return c;
    }

    public static double times(double[] a, double[] b) {
        double res = 0;
        if (a.length != b.length) {
            System.exit(1);
            return 0;
        }
        for (int i = 0; i < a.length; i++) {
            res += a[i] * b[i];
        }
        return res;
    }

    public static double[] ones(int length) {
        double[] l = new double[length];
        for (int i = 0; i < length; i++) {
            l[i] = 1;
        }
        return l;
    }

    public static double[] dotDivision(double[] a, double[] b) {
        if (a.length != b.length) {
            return null;
        }
        double[] c = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i] / b[i];
        }
        return c;
    }

    public static double[] dotDivision(double a, double[] b) {

        double[] c = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = a / b[i];
        }
        return c;
    }

    public static double[] dotDivision(double[] b, double a) {

        double[] c = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = b[i] / a;
        }
        return c;
    }

    public static double[] minus(double[] a, double[] b) {
        if (a.length != b.length) {
            return null;
        }
        double[] c = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = a[i] - b[i];
        }
        return c;
    }

    public static double[] minus(double[] b, double a) {

        double[] c = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = b[i] - a;
        }
        return c;
    }

    public static double[] minus(double a, double[] b) {

        double[] c = new double[b.length];
        for (int i = 0; i < b.length; i++) {
            c[i] = a - b[i];
        }
        return c;
    }

    public static double[] dotSquare(double[] a, double b) {
        double[] c = new double[a.length];
        for (int i = 0; i < a.length; i++) {
            c[i] = Math.pow(a[i], b);

        }
        return c;
    }

    public static double[] linspace(double x1, double x2, int n) {
        double[] values = new double[n];
        double dx = (x2 - x1) / (double) (n - 1);
        for (int i = 0; i < n; i++) {
            values[i] = x1 + ((double) i) * dx;
        }
        return values;
    }

    public static int findFirst(double[] x, double yl) {
        int i;
        if (x[x.length - 1] < yl) {
            return x.length - 2;
        }
        for (i = 0; i < x.length - 1; i++) {
            if (x[i + 1] >= yl) {
                break;
            }
        }
        return i;
    }

    public static double max(double[] x) {
        double max = x[0];
        for (int i = 1; i < x.length; i++) {
            if (x[i] > max) {
                max = x[i];
            }
        }
        return max;
    }

    public static int max(int[] x) {
        int max = x[0];
        for (int i = 1; i < x.length; i++) {
            if (x[i] > max) {
                max = x[i];
            }
        }
        return max;
    }

    public static double min(double[] x) {
        double min = x[0];
        for (int i = 1; i < x.length; i++) {
            if (x[i] < min) {
                min = x[i];
            }
        }
        return min;
    }

    public static int min(int[] x) {
        int min = x[0];
        for (int i = 1; i < x.length; i++) {
            if (x[i] < min) {
                min = x[i];
            }
        }
        return min;
    }

    public static int[] find(double[] x, double e, boolean status) {
        // status: true for x>e
        //	       false for x<e
        // return the index of x that satisfy the equation
        int sum = 0;
        List index = new ArrayList();
        if (status) {
            for (int i = 0; i < x.length; i++) {
                if (x[i] > e) {
                    sum++;
                    index.add(i);
                }
            }
        } else {
            for (int i = 0; i < x.length; i++) {
                if (x[i] < e) {
                    sum++;
                    index.add(i);
                }
            }
        }
        int[] res = new int[sum];
        for (int i = 0; i < sum; i++) {
            res[i] = (int) index.get(i);
        }
        return res;
    }

    public static int[] findEqual(double[] x, double e) {
        // status: true for x>e
        //	       false for x<e
        // return the index of x that satisfy the equation
        int sum = 0;
        List index = new ArrayList();

        for (int i = 0; i < x.length; i++) {
            if (x[i] == e) {
                sum++;
                index.add(i);
            }
        }

        int[] res = new int[sum];
        for (int i = 0; i < sum; i++) {
            res[i] = (int) index.get(i);
        }
        return res;
    }

    public static double[] delete(double[] x, int[] index) {
        if (index.length == 0) {
            return x;
        }
        double[] res = new double[x.length - index.length];
        int t = 0;
        int y = 0;
        for (int i = 0; i < x.length; i++) {
            if (i != index[t]) {
                res[y++] = x[i];
            } else {
                t++;
                if (t >= index.length) {
                    t = index.length - 1;
                }
            }
        }
        return res;
    }

    public static double[] delete(double[] x, int index) {
        double[] res = new double[x.length - 1];
        int t = 0;
        int y = 0;
        for (int i = 0; i < x.length; i++) {
            if (i != index) {
                res[y++] = x[i];
            }
        }
        return res;
    }

    public static int[] argSort(double[] x) {
        // 升序排列x并返回对应索引
        int[] index = new int[x.length];
        for (int i = 0; i < x.length; i++) {
            index[i] = i;
        }
//    		for(int i=0;i<x.length-1;i++) {
//    			for(int j=i;j<x.length;j++) {
//    				if(x[i]>x[j]) {
//    					double tmp=x[j];
//    					x[j]=x[i];
//    					x[i]=tmp;
//    					int tmp1=index[j];
//    					index[j]=index[i];
//    					index[i]=tmp1;
//    				}
//    			}
//    		}
        double[] xx = x.clone();
        quickSort(xx, index, 0, x.length - 1);
        return index;
    }

    public static int[] sort(double[] x) {
        // 升序排列x并返回对应索引
        int[] index = new int[x.length];
        for (int i = 0; i < x.length; i++) {
            index[i] = i;
        }
//    		for(int i=0;i<x.length-1;i++) {
//    			for(int j=i;j<x.length;j++) {
//    				if(x[i]>x[j]) {
//    					double tmp=x[j];
//    					x[j]=x[i];
//    					x[i]=tmp;
//    					int tmp1=index[j];
//    					index[j]=index[i];
//    					index[i]=tmp1;
//    				}
//    			}
//    		}
        quickSort(x, index, 0, x.length - 1);
        return index;
    }

    public static double[] sortByIndex(double[] x, int[] index) {
        double[] res = new double[index.length];
        for (int i = 0; i < index.length; i++) {
            res[i] = x[index[i]];
        }
        return res;
    }

    public static double sum(double[] x) {
        double sum = 0;
        for (int i = 0; i < x.length; i++) {
            sum += x[i];
        }
        return sum;
    }

    public static double[] part(double[] x, int a, int b) {
        // In python, equals to x[a:b]
        double[] res = new double[b - a];
        for (int i = a; i < b; i++) {
            res[i - a] = x[i];
        }
        return res;
    }

    public static double[] connect(double[] a, double[] b) {
        double[] res = new double[a.length + b.length];
        for (int i = 0; i < a.length; i++) {
            res[i] = a[i];
        }
        for (int i = 0; i < b.length; i++) {
            res[i + a.length] = b[i];
        }
        return res;
    }

    public static double[] zeros(int x) {
        double[] res = new double[x];
        for (int i = 0; i < x; i++) {
            res[i] = 0;
        }
        return res;
    }

    public static double[][] zeros(int x, int y) {
        double[][] res = new double[x][y];
        for (int i = 0; i < x; i++) {
            for (int j = 0; j < y; j++) {
                res[i][j] = 0;
            }
        }
        return res;
    }

    public static double[] reverse(double[] x) {
        double[] res = new double[x.length];
        for (int i = 0; i < x.length; i++) {
            res[i] = x[x.length - i - 1];
        }
        return res;
    }

    public static double[] cumsum(double[] x, boolean order) {
        //order: true for positive order
        // false for reverse order
        double[] res = zeros(x.length);
        double n = 0;
        if (!order) {
            x = reverse(x);
        }
        for (int i = 0; i < x.length; i++) {
            n += x[i];
            res[i] = n;
        }
        return res;
    }

    public static double[] cumsum(double[] x) {
        //order: true for positive order
        // false for reverse order
        double[] res = zeros(x.length);
        double n = 0;
        for (int i = 0; i < x.length; i++) {
            n += x[i];
            res[i] = n;
        }
        return res;
    }

    public static void assign(double[] x, int a, int b, double[] y) {
        if (b - a != y.length) {
            System.err.println("Can't assign " + y.length + " to " + (b - a));
            System.exit(1);
        }
        for (int i = a; i < b; i++) {
            x[i] = y[i - a];
        }
    }

    public static void shape(double[] x) {
        System.out.println("shape:" + x.length);
    }

    public static void printArray(List l) {
        for (int i = 0; i < l.size(); i++) {
            System.out.print(l.get(i) + "\t");
        }
        System.out.print("\n");
    }

    public static double[] diff(double[] x) {
        // TODO Auto-generated method stub
        double[] res = new double[x.length - 1];
        for (int i = 0; i < res.length; i++) {
            res[i] = x[i + 1] - x[i];
        }
        return res;
    }

    public static int index(double[] x, double a) {
        for (int i = 0; i < x.length; i++) {
            if (x[i] == a) {
                return i;
            }
        }
        return -1;
    }

    public static void set(double[] diff, int[] index, int i) {
        // TODO Auto-generated method stub
        for (int j = 0; j < index.length; j++) {
            diff[index[j]] = i;
        }
    }

    public static void shape(double[][] x) {
        // TODO Auto-generated method stub
        System.out.println("shape:" + "[" + x.length + "," + x[0].length + "]");

    }

    public static void quickSort(double[] x, int[] index, int low, int high) {
        if (low < high) {
            int middle = getMiddle(x, index, low, high);
            quickSort(x, index, low, middle - 1);
            quickSort(x, index, middle + 1, high);
        }
    }

    private static int getMiddle(double[] x, int[] index, int low, int high) {
        double tmp = x[low];
        int indextmp = index[low];
        while (low < high) {
            while (low < high && x[high] > tmp) {
                high--;
            }
            x[low] = x[high];
            index[low] = index[high];
            while (low < high && x[low] < tmp) {
                low++;
            }
            x[high] = x[low];
            index[high] = index[low];
        }
        x[low] = tmp;
        index[low] = indextmp;
        return low;
    }
}
