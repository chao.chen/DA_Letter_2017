package Algorithms;

public class KM {

    public static double[] run(double[] xl, double[] xr, double[] wl, double[] wr, boolean sort) {
        double y;
        double yl;
        double yr;
        int l = 0;
        int r = 0;
        int ly = xl.length;
        if (utils.max(wl) == 0) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        double epsilon = 10e-10f;
        int[] index = utils.find(wr, epsilon, false);
        if (index.length == ly) {
            yl = utils.min(xl);
            yr = utils.max(xl);
            y = (yl + yr) / 2;
            l = 1;
            r = ly - 1;
            return new double[]{y, yl, yr, l, r};
        }
        xl = utils.delete(xl, index);
        xr = utils.delete(xr, index);
        wl = utils.delete(wl, index);
        wr = utils.delete(wr, index);
        ly = xl.length;
        if (sort) {
            int[] i_ = utils.argSort(xl);
            xl = utils.sortByIndex(xl, i_);
            xr = utils.sortByIndex(xr, i_);
            wr = utils.sortByIndex(wr, i_);
            wl = utils.sortByIndex(wl, i_);
        }
        double wl2[] = wl.clone();
        double wr2[] = wr.clone();

        ly = xl.length;
        if (ly == 1) {
            yl = xl[0];
            l = 1;

        } else {
            double[] f = utils.dotDivision(utils.dotPlus(wl, wr), 2);
            double y0 = utils.times(xl, f) / utils.sum(f);
            yl = y0 + 1;
            double k = 0;
            while (Math.abs(yl - y0) > epsilon && k < ly) {
                yl = y0;
                k += 1;
                l = utils.findFirst(xl, y0);
                f = utils.connect(utils.part(wr, 0, l + 1), utils.part(wl, l + 1, ly));
                y0 = utils.times(xl, f) / utils.sum(f);
            }
        }
        if (sort) {
            int[] i_ = utils.argSort(xr);
            xr = utils.sortByIndex(xr, i_);
            wl = utils.sortByIndex(wl2, i_);
            wr = utils.sortByIndex(wr2, i_);
        }

        ly = xr.length;
        if (ly == 1) {
            yr = xr[0];
            r = 1;
        } else {
            double[] f = utils.dotDivision(utils.dotPlus(wl, wr), 2);
            double y0 = utils.times(xr, f) / utils.sum(f);
            yr = y0 + 1;
            double k = 0;
            while (Math.abs(yr - y0) > epsilon && k < ly) {
                yr = y0;
                k += 1;
                r = utils.findFirst(xr, y0);
                f = utils.connect(utils.part(wl, 0, r + 1), utils.part(wr, r + 1, ly));
                y0 = utils.times(xr, f) / utils.sum(f);
            }
        }
        y = (yl + yr) / 2;
        return new double[]{y, yl, yr, (double) l, (double) r};
    }

}