import Algorithms.*;
import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLDouble;

import java.io.IOException;
import java.util.ArrayList;


public class TestBell {

    public static void example1() throws IOException {

        int m = 5000;
        int[] N = new int[]{10, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000};
        boolean sort = false;
        long startTime;
        long endTime;
        double[][] res = new double[6][m];
        double[][] times = new double[6][N.length];

        // Test Part===================

        // ============================
        for (int k = 0; k < N.length; k++) {
            // For test
            // Begin testing six algorithm
            System.out.println("Length of Nk : " + N[k]);
            double[] al = utils.dotPlus(1, utils.rand(m));
            double[] b = utils.ones(m);
            double[] au = utils.dotTimes(al, utils.dotPlus(1, utils.rand(m)));
            double[] c = utils.dotTimes(10, utils.rand(m));
            double[] x = utils.linspace(0.0, 10.0, N[k]);
            double[][] mul = new double[m][N[k]];
            double[][] muu = new double[m][N[k]];
            for (int i = 0; i < m; i++) {
                mul[i] = utils.dotDivision(1, utils.dotPlus(1, utils.dotSquare(utils.dotDivision(utils.minus(x, c[i]), al[i]), 2 * b[i])));
                muu[i] = utils.dotDivision(1, utils.dotPlus(1, utils.dotSquare(utils.dotDivision(utils.minus(x, c[i]), au[i]), 2 * b[i])));

            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[0][i] = KM.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[0][k] = (double) (endTime - startTime) / 1000;

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[1][i] = EKM.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[1][k] = (double) (endTime - startTime) / 1000;

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[2][i] = EIASC.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[2][k] = (double) (endTime - startTime) / 1000;

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[3][i] = DA.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[3][k] = (double) (endTime - startTime) / 1000;

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[4][i] = DA_STAR.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[4][k] = (double) (endTime - startTime) / 1000;

/*
            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[5][i] = DAND.run(x, x, mul[i], muu[i], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[5][k] = (double) (endTime - startTime) / 1000;
*/

            // Begin test
            for (int j = 0; j < m; j++) {
                double[] tmp = new double[6];
                for (int i = 0; i < 6; i++) {
                    tmp[i] = res[i][j];
                }
                double[] diff = utils.diff(tmp);
                double epsilon = 10e-10f;
                int[] index = utils.find(diff, epsilon, false);
                utils.set(diff, index, 0);
                if (utils.max(diff) != 0) {
                    System.out.println("error! On res[" + j + "]");
                    utils.printArray(diff);
                }
            }


        }
        for (int i = 0; i < 6; i++) {
            utils.printArray(times[i]);
        }

        // Writing array to mat
        MLDouble mlr = new MLDouble("results", res);
        MLDouble mlt = new MLDouble("times", times);
        ArrayList l2mat = new ArrayList();
        l2mat.add(mlr);
        l2mat.add(mlt);
        new MatFileWriter("JavaExample1.mat", l2mat);

    }

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {
        example1();
    }

}
