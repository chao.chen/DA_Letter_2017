import Algorithms.*;
import com.jmatio.io.MatFileWriter;
import com.jmatio.types.MLDouble;

import java.io.IOException;
import java.util.ArrayList;

public class TestRandom {

    public static void example2() throws IOException {
        int m = 5000;
        int[] N = new int[]{10, 200, 400, 600, 800, 1000, 1200, 1400, 1600, 1800, 2000};
        boolean sort = false;
        long startTime;
        long endTime;
        double[][] res = new double[6][m];
        double[][] times = new double[6][N.length];

        for (int k = 0; k < N.length; k++) {
            // For test
            // Begin testing six algorithm
            System.out.println("Length of Nk : " + N[k]);
            double[][] Xs = utils.rand(m, N[k]);
            for (int i = 0; i < m; i++) {
                utils.sort(Xs[i]);
            }
            double[][] Fs = utils.zeros(2 * m, N[k]);
            for (int i = 1; i < Fs.length; i += 2) {
                Fs[i] = utils.rand(N[k]);
            }
            for (int i = 0; i < Fs.length; i += 2) {
                Fs[i] = utils.dotTimes(utils.rand(N[k]), Fs[i + 1]);
            }

            try {
                Thread.sleep(10000);
            } catch (InterruptedException ex) {
                Thread.currentThread().interrupt();
            }

            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[0][i] = KM.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[0][k] = (double) (endTime - startTime) / 1000;


            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[1][i] = EKM.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[1][k] = (double) (endTime - startTime) / 1000;


            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[2][i] = EIASC.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[2][k] = (double) (endTime - startTime) / 1000;


            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[3][i] = DA.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[3][k] = (double) (endTime - startTime) / 1000;


            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[4][i] = DA_STAR.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[4][k] = (double) (endTime - startTime) / 1000;

/*
            startTime = System.currentTimeMillis();
            for (int i = 0; i < m; i++) {
                res[5][i] = DAND.run(Xs[i], Xs[i], Fs[2 * i], Fs[2 * i + 1], sort)[0];
            }
            endTime = System.currentTimeMillis();
            times[5][k] = (double) (endTime - startTime) / 1000;
*/

            // Begin test
            for (int j = 0; j < m; j++) {
                double[] tmp = new double[6];
                for (int i = 0; i < 6; i++) {
                    tmp[i] = res[i][j];
                }
                double[] diff = utils.diff(tmp);
                double epsilon = 10e-10f;
                int[] index = utils.find(diff, epsilon, false);
                utils.set(diff, index, 0);
                if (utils.max(diff) != 0) {
                    System.out.println("error! On res[" + j + "]");
//					utils.printArray(diff);
                    utils.printArray(tmp);
                    System.exit(0);
                }
            }

        }
        for (int i = 0; i < 6; i++) {
            utils.printArray(times[i]);
        }
        // Writing array to mat
        MLDouble mlr = new MLDouble("results", res);
        MLDouble mlt = new MLDouble("times", times);
        ArrayList l2mat = new ArrayList();
        l2mat.add(mlr);
        l2mat.add(mlt);
        new MatFileWriter("JavaExample2.mat", l2mat);
    }

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {
        example2();
    }
}
